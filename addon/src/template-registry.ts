// Easily allow apps, which are not yet using strict mode templates, to consume your Glint types, by importing this file.
// Add all your components, helpers and modifiers to the template registry here, so apps don't have to do this.
// See https://typed-ember.gitbook.io/glint/environments/ember/authoring-addons

import type FieldMessagesComponent from './components/field-messages';
import type InputFieldComponent from './components/input-field';
import type InputFieldCheckboxComponent from './components/input-field/checkbox';
import type InputFieldCurrencyComponent from './components/input-field/currency';
import type InputFieldEmailComponent from './components/input-field/email';
import type InputFieldNumberComponent from './components/input-field/number';
import type InputFieldPercentComponent from './components/input-field/percent';
import type InputFieldPriceComponent from './components/input-field/price';
import type InputFieldSwitchComponent from './components/input-field/switch';
import type InputFieldTextComponent from './components/input-field/text';
import type InputFieldTextareaComponent from './components/input-field/textarea';
import type InputFieldUrlComponent from './components/input-field/url';
import type InputCheckboxComponent from './components/input/checkbox';
import type InputMultiSelectComponent from './components/input/multi-select';
import type InputNumberComponent from './components/input/number';
import type InputRangeComponent from './components/input/range';
import type InputSelectComponent from './components/input/select';
import type InputTextComponent from './components/input/text';
import type InputTextareaComponent from './components/input/textarea';
import type OutputFieldComponent from './components/output-field';
import type OutputFieldCheckboxComponent from './components/output-field/checkbox';
import type OutputFieldDateComponent from './components/output-field/date';
import type OutputFieldDatetimeComponent from './components/output-field/datetime';
import type OutputFieldNumberComponent from './components/output-field/number';
import type OutputFieldPercentComponent from './components/output-field/percent';
import type OutputFieldPreComponent from './components/output-field/pre';
import type OutputFieldPriceComponent from './components/output-field/price';
import type OutputFieldSwitchComponent from './components/output-field/switch';
import type OutputFieldTextComponent from './components/output-field/text';
import type OutputCheckboxComponent from './components/output/checkbox';
import type OutputDateRangeComponent from './components/output/date-range';
import type OutputDateComponent from './components/output/date';
import type OutputNumberComponent from './components/output/number';
import type OutputPercentComponent from './components/output/percent';
import type OutputPreComponent from './components/output/pre';
import type OutputPriceComponent from './components/output/price';
import type OutputSelectComponent from './components/output/select';
import type OutputTextComponent from './components/output/text';
import type dateAdd from './helpers/date-add';
import type DateFormatHelper from './helpers/date-format';
import type dateSubtract from './helpers/date-subtract';
import type FieldHelptextHelper from './helpers/field-helptext';
import type FieldLabelHelper from './helpers/field-label';

export default interface Registry {
  // Components
  FieldMessages: typeof FieldMessagesComponent;
  InputField: typeof InputFieldComponent;
  'InputField::Checkbox': typeof InputFieldCheckboxComponent;
  'InputField::Currency': typeof InputFieldCurrencyComponent;
  'InputField::Email': typeof InputFieldEmailComponent;
  'InputField::Number': typeof InputFieldNumberComponent;
  'InputField::Percent': typeof InputFieldPercentComponent;
  'InputField::Price': typeof InputFieldPriceComponent;
  'InputField::Switch': typeof InputFieldSwitchComponent;
  'InputField::Text': typeof InputFieldTextComponent;
  'InputField::Textarea': typeof InputFieldTextareaComponent;
  'InputField::Url': typeof InputFieldUrlComponent;
  'Input::Checkbox': typeof InputCheckboxComponent;
  'Input::MultiSelect': typeof InputMultiSelectComponent;
  'Input::Number': typeof InputNumberComponent;
  'Input::Range': typeof InputRangeComponent;
  'Input::Select': typeof InputSelectComponent;
  'Input::Text': typeof InputTextComponent;
  'Input::Textarea': typeof InputTextareaComponent;
  OutputField: typeof OutputFieldComponent;
  'OutputField::Checkbox': typeof OutputFieldCheckboxComponent;
  'OutputField::Date': typeof OutputFieldDateComponent;
  'OutputField::Datetime': typeof OutputFieldDatetimeComponent;
  'OutputField::Number': typeof OutputFieldNumberComponent;
  'OutputField::Percent': typeof OutputFieldPercentComponent;
  'OutputField::Pre': typeof OutputFieldPreComponent;
  'OutputField::Price': typeof OutputFieldPriceComponent;
  'OutputField::Switch': typeof OutputFieldSwitchComponent;
  'OutputField::Text': typeof OutputFieldTextComponent;
  'Output::Checkbox': typeof OutputCheckboxComponent;
  'Output::DateRange': typeof OutputDateRangeComponent;
  'Output::Date': typeof OutputDateComponent;
  'Output::Number': typeof OutputNumberComponent;
  'Output::Percent': typeof OutputPercentComponent;
  'Output::Pre': typeof OutputPreComponent;
  'Output::Price': typeof OutputPriceComponent;
  'Output::Select': typeof OutputSelectComponent;
  'Output::Text': typeof OutputTextComponent;
  // Helpers
  'date-add': typeof dateAdd;
  'date-format': typeof DateFormatHelper;
  'date-subtract': typeof dateSubtract;
  'field-helptext': typeof FieldHelptextHelper;
  'field-label': typeof FieldLabelHelper;
}
