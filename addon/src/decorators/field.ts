import 'reflect-metadata';
import type { ComponentLike } from '@glint/template';
import type { BaseInputFieldOptions } from '../components/input-field/-base.gts';
import { assert } from '@ember/debug';
import type { FieldOf, SomeModel } from '../index.ts';
import type { SomeInputFieldSignature } from '../components/input-field.ts';
import type { SomeOutputFieldSignature } from '../components/output-field.ts';
import type { BaseOutputFieldOptions } from '../components/output-field/-base.ts';

export const METADATA_KEY = `efc_fieldoptions`;

export const BaseFieldOptionsKeys = ['label', 'prefix', 'suffix'];

/**
 * Base options for input and output fields: label / prefix / suffix
 */
export interface BaseFieldOptions {
  /**
   * The label to be displayed with the input / output.
   */
  label?: string;
  /**
   * The prefix to be prepended to the input / output.
   */
  prefix?: string | string[];
  /**
   * The suffix to be appended to the input / output.
   */
  suffix?: string | string[];
}

export interface FieldDecoratorOptions<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  type: string;
  inputField?: ComponentLike<SomeInputFieldSignature<O, F>>;
  inputFieldOptions?: BaseInputFieldOptions;
  outputField?: ComponentLike<SomeOutputFieldSignature<O, F>>;
  outputFieldOptions?: BaseOutputFieldOptions;
}

export default function field<
  O extends SomeModel = SomeModel,
  F extends FieldOf<O> = FieldOf<O>,
>(options: FieldDecoratorOptions<O, F>): PropertyDecorator {
  return (target: object, propertyKey: string | symbol) => {
    assert(
      'You can only apply a field() decorator to a property of type "string"',
      typeof propertyKey === 'string',
    );

    registerFieldOptions({
      target,
      propertyKey: propertyKey,
      options,
    });
  };
}

function registerFieldOptions<O extends SomeModel, F extends FieldOf<O>>(meta: {
  target: object;
  propertyKey: string | symbol;
  options: FieldDecoratorOptions<O, F>;
}) {
  const { target, propertyKey, options } = meta;
  // Create a key 'efc_fieldoptions:someProperty'
  // Store this metadata in Reflect metadata
  Reflect.defineMetadata(METADATA_KEY, options, target, propertyKey);
}
