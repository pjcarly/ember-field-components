import Component from '@glimmer/component';
import type { ComponentLike } from '@glint/template';
import InputFieldTextComponent from './input-field/text.gts';
import { service } from '@ember/service';
import type FieldComponentsService from '../services/field-components';
import type { FieldOf, SomeModel } from '../index.ts';
import type { InputFieldArguments } from './input-field/-base.gts';
import { cached } from '@glimmer/tracking';

export interface SomeInputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: InputFieldArguments<O, F>;
  Element: HTMLElement;
}

type InputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> = SomeInputFieldSignature<O, F>;

/**
 * The default InputField.
 *
 * Looks up the options passed to the @field() decorator to decide on:
 * - Which input field should be rendered? (text, number...)
 * - Which input options should be passed on?
 * - ...
 * Most of these options can be overwritten arguments.
 *
 * When there are no field options defined, it will render an InputField::Text with no default options.
 */
export default class InputFieldComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<InputFieldSignature<O, F>> {
  @service declare fieldComponents: FieldComponentsService;

  private get inputFieldComponent(): ComponentLike<
    SomeInputFieldSignature<O, F>
  > {
    // Always fallback to a text input field: make it easy for the developer to add inputs without adding a field decorator.
    return this.fieldDecoratorOptions?.inputField ?? InputFieldTextComponent;
  }

  /**
   * An input field is disabled when the 'readonly' property is set on the InputFieldOptions.
   * This can also be overwritten using the '@disabled' argument.
   */
  private get disabled() {
    if (this.args.disabled !== undefined) {
      return this.args.disabled;
    }

    if (this.args.readonly !== undefined) {
      return this.args.readonly;
    }

    return this.inputFieldOptions?.readonly;
  }

  /**
   * An input field is required when the 'required' property is set on the InputFieldOptions.
   * This can also be overwritten using the '@required' argument.
   */
  private get required() {
    if (this.args.required !== undefined) {
      return this.args.required;
    }

    const { conditionalRequired } = this.inputFieldOptions ?? {};
    if (conditionalRequired) {
      if (
        (Array.isArray(conditionalRequired)
          ? conditionalRequired
          : [conditionalRequired]
        )
          .map((key) => {
            /**
             * We need to get all properties to make sure this required() getter is reevaluated when one of the properties has changed.
             */
            return !!this.args.model[key];
          })
          .includes(true)
      ) {
        return true;
      }
    }

    return this.inputFieldOptions?.required;
  }

  /**
   * Get the options passed to the @field() decorator on the field.
   */
  @cached
  private get fieldDecoratorOptions() {
    return this.fieldComponents.getFieldOptions(
      this.args.model,
      this.args.field,
    );
  }

  private get inputFieldOptions() {
    return this.fieldDecoratorOptions?.inputFieldOptions;
  }

  /**
   * Options to be passed to the input.
   * These are the options defined on the decorator and can be overwritten by the options passed by '@inputOptions' argument.
   */
  private get inputOptions() {
    return {
      // Options from the decorator
      ...this.inputFieldOptions?.inputOptions,
      // Options can be overwritten by options passed by args
      ...this.args.inputOptions,
    };
  }

  private get prefixes(): string[] {
    const prefixes = [];

    // Argument prefixes first
    if (this.args.prefix) {
      prefixes.push(
        ...(Array.isArray(this.args.prefix)
          ? this.args.prefix
          : [this.args.prefix]),
      );
    }

    // Followed by Input Field Options prefix(es)
    if (this.inputFieldOptions?.prefix) {
      prefixes.push(
        ...(Array.isArray(this.inputFieldOptions.prefix)
          ? this.inputFieldOptions.prefix
          : [this.inputFieldOptions.prefix]),
      );
    }

    return prefixes;
  }

  private get suffixes(): string[] {
    const suffixes = [];

    // Input Field Options suffix(es) first
    if (this.inputFieldOptions?.suffix) {
      suffixes.push(
        ...(Array.isArray(this.inputFieldOptions.suffix)
          ? this.inputFieldOptions.suffix
          : [this.inputFieldOptions.suffix]),
      );
    }

    // Followed by argument suffix(es)
    if (this.args.suffix) {
      suffixes.push(
        ...(Array.isArray(this.args.suffix)
          ? this.args.suffix
          : [this.args.suffix]),
      );
    }

    return suffixes;
  }

  <template>
    <this.inputFieldComponent
      @model={{@model}}
      @field={{@field}}
      @disabled={{this.disabled}}
      @required={{this.required}}
      @valueChanged={{@valueChanged}}
      @preSetHook={{@preSetHook}}
      @inline={{@inline}}
      @inputOptions={{this.inputOptions}}
      @inputId={{@inputId}}
      @label={{@label}}
      @prefix={{this.prefixes}}
      @suffix={{this.suffixes}}
      @showErrors={{@showErrors}}
      @layout={{@layout}}
      ...attributes
    />
  </template>
}
