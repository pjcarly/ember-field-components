import Component from '@glimmer/component';
import OutputCheckboxComponent, {
  type OutputCheckboxOptions,
  type OutputCheckboxSignature,
} from '../output/checkbox.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldSwitchComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputCheckboxOptions>;
  Element: OutputCheckboxSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputCheckboxComponent}}
      @fieldType='switch'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
