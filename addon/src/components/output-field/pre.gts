import Component from '@glimmer/component';
import OutputPreComponent, {
  type OutputPreOptions,
  type OutputPreSignature,
} from '../output/pre.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldPreComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputPreOptions>;
  Element: OutputPreSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputPreComponent}}
      @fieldType='pre'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
