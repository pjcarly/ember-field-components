import Component from '@glimmer/component';
import OutputNumberComponent, {
  type OutputNumberOptions,
  type OutputNumberSignature,
} from '../output/number.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldNumberComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputNumberOptions>;
  Element: OutputNumberSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputNumberComponent}}
      @fieldType='number'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
