import Component from '@glimmer/component';
import OutputPercentComponent, {
  type OutputPercentOptions,
  type OutputPercentSignature,
} from '../output/percent.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldPercentComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputPercentOptions>;
  Element: OutputPercentSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputPercentComponent}}
      @fieldType='percent'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
