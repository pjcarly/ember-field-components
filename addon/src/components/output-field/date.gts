import Component from '@glimmer/component';
import OutputDateComponent, {
  type OutputDateOptions,
  type OutputDateSignature,
} from '../output/date.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldDateComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDateOptions>;
  Element: OutputDateSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDateComponent}}
      @fieldType='date'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
