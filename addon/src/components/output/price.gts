import Component from '@glimmer/component';
import type { BaseOutputArguments } from './-base';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import type FieldInformationService from '../../services/field-information';
import { isNone } from '@ember/utils';
import { isInfinity, type OutputNumberOptions } from './number.gts';

export interface OutputPriceOptions extends OutputNumberOptions {
  currency?: string;
}

export interface OutputPriceArguments extends BaseOutputArguments {
  value: number | undefined;
  options?: OutputPriceOptions;
}

export interface OutputPriceSignature {
  Args: OutputPriceArguments;
  Element: HTMLSpanElement;
}

export default class OutputPriceComponent extends Component<OutputPriceSignature> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    if (isInfinity(this.args.value, this.args.options)) {
      return `${this.args.value < 0 ? '-' : '+'}∞`;
    }

    return this.intl.formatNumber(this.args.value!, {
      style: 'currency',
      currency:
        this.args.options?.currency ?? this.fieldInformation.defaultCurrency,
      currencyDisplay: this.fieldInformation.currencyDisplay,
    });
  }

  <template>
    <span
      class='output output-price{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >
      {{this.value}}
    </span>
  </template>
}
