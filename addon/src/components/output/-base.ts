/* eslint-disable @typescript-eslint/no-explicit-any */
export interface BaseOutputArguments {
  value?: any;
}

export interface SomeOutputArguments<
  IO extends Record<never, never> = Record<never, never>,
> {
  value: any | undefined;
  options: IO | undefined;
}
