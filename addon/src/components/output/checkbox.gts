import type { TOC } from '@ember/component/template-only';
import type { BaseOutputArguments } from './-base';
import { isNone } from '@ember/utils';

export interface OutputCheckboxOptions {}

export interface OutputCheckboxArguments extends BaseOutputArguments {
  value: boolean | undefined;
}

export interface OutputCheckboxSignature {
  Args: OutputCheckboxArguments;
  Element: HTMLSpanElement;
}

const OutputCheckboxComponent: TOC<OutputCheckboxSignature> = <template>
  {{#let (isNone @value) as |isEmpty|}}
    <span
      class='output output-checkbox{{if isEmpty " output--empty"}}'
      ...attributes
    >
      {{#unless isEmpty}}
        {{! template-lint-disable require-input-label  }}
        <input
          type='checkbox'
          class='output-checkbox__checkbox'
          checked={{@value}}
          disabled={{true}}
        />
      {{/unless}}
    </span>
  {{/let}}
</template>;
export default OutputCheckboxComponent;
