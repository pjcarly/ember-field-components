import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information';
import Component from '@glimmer/component';
import { format, isValid } from 'date-fns';
import IntlService from 'ember-intl/services/intl';
import type { BaseOutputArguments } from './-base';
import { isNone } from '@ember/utils';

export interface OutputDateRangeOptions {
  displayTime?: boolean;
}

export interface OutputDateRangeArguments extends BaseOutputArguments {
  value: [Date, Date] | undefined;
  options?: OutputDateRangeOptions;
}

export interface OutputDateRangeSignature {
  Args: OutputDateRangeArguments;
  Element: HTMLSpanElement;
}

export default class OutputDateRangeComponent extends Component<OutputDateRangeSignature> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    const someDateInvalid = this.args.value.some((date) => !isValid(date));

    if (someDateInvalid) {
      return this.intl.t('output.date.range.invalid');
    }

    return this.args.value
      .map((date) => {
        return format(
          date,
          this.args.options?.displayTime
            ? this.fieldInformation.dateTimeFormat
            : this.fieldInformation.dateFormat,
        );
      })
      .join(` ${this.intl.t('output.date.range.separator')} `);
  }

  <template>
    <span
      class='output output-date-range{{if
          (isNone this.value)
          " output--empty"
        }}'
      ...attributes
    >{{this.value}}</span>
  </template>
}
