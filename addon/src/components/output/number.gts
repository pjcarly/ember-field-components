import Component from '@glimmer/component';
import type { BaseOutputArguments } from './-base';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { isNone } from '@ember/utils';

export interface OutputNumberOptions {
  decimals?: number;
  precision?: number;
}

export interface OutputNumberArguments extends BaseOutputArguments {
  value: number | undefined;
  options?: OutputNumberOptions;
}

export interface OutputNumberSignature {
  Args: OutputNumberArguments;
  Element: HTMLSpanElement;
}

export const isInfinity = (
  value: number,
  options?: {
    precision?: number;
    decimals?: number;
  },
) => {
  if (!options?.precision) {
    return;
  }

  if (!options.decimals) {
    options.decimals = 0;
  }

  const infinity =
    Math.pow(10, options.precision - options.decimals) -
    Math.pow(10, -options.decimals);

  return Math.abs(value) === infinity;
};

export default class OutputNumberComponent extends Component<OutputNumberSignature> {
  @service declare intl: IntlService;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    // if (this.args.options?.decimals !== undefined) {
    //   return this.args.value.toFixed(this.args.options.decimals);
    // }

    if (isInfinity(this.args.value, this.args.options)) {
      return `${this.args.value < 0 ? '-' : '+'}∞`;
    }

    return this.args.value.toString();
  }

  <template>
    <span
      class='output output-number{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >{{this.value}}</span>
  </template>
}
