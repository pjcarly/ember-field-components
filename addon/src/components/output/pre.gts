import type { TOC } from '@ember/component/template-only';
import type { BaseOutputArguments } from './-base';
import { isNone } from '@ember/utils';

export interface OutputPreOptions {}

export interface OutputPreArguments extends BaseOutputArguments {
  value: string | undefined;
  options?: OutputPreOptions;
}

export interface OutputPreSignature {
  Args: OutputPreArguments;
  Element: HTMLSpanElement;
}

const OutputPreComponent: TOC<OutputPreSignature> = <template>
  <span
    class='output output-pre{{if (isNone @value) " output--empty"}}'
    ...attributes
  >
    {{#if @value}}
      {{! @prettier-ignore (do not format, we don't want excess whitespace in the pre) }}
      <pre>{{@value}}</pre>
    {{/if}}
  </span>
</template>;
export default OutputPreComponent;
