import Component from '@glimmer/component';
import type { BaseOutputArguments } from './-base';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { isNone } from '@ember/utils';
import { isInfinity, type OutputNumberOptions } from './number.gts';

export interface OutputPercentOptions extends OutputNumberOptions {}

export interface OutputPercentArguments extends BaseOutputArguments {
  value: number | undefined;
  options?: OutputPercentOptions;
}

export interface OutputPercentSignature {
  Args: OutputPercentArguments;
  Element: HTMLSpanElement;
}
export default class OutputPercentComponent extends Component<OutputPercentSignature> {
  @service declare intl: IntlService;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    // if (this.args.options?.decimals !== undefined) {
    //   return this.args.value.toFixed(this.args.options.decimals) + '%';
    // }

    if (isInfinity(this.args.value, this.args.options)) {
      return `${this.args.value < 0 ? '-' : '+'}∞`;
    }

    return this.args.value.toString() + '%';
  }

  <template>
    <span
      class='output output-percent{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >{{this.value}}</span>
  </template>
}
