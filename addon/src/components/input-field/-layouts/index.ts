import type { ComponentLike } from '@glint/template';

export interface InputFieldLayoutSignature<E extends Element = HTMLElement> {
  Args: {
    inputId: string | undefined;
    label: string | undefined;
    disabled: boolean | undefined;
    required: boolean | undefined;
    prefixes: string[] | undefined;
    suffixes: string[] | undefined;
    helptext: string | undefined;
    errors: string[] | undefined;
    inputComponent: ComponentLike<{ Element: E }>;
  };
  Element: E;
}

export type SomeInputFieldLayout = ComponentLike<InputFieldLayoutSignature>;
