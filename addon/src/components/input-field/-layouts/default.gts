import { guidFor } from '@ember/object/internals';
import type { InputFieldLayoutSignature } from './index.ts';
import Component from '@glimmer/component';

export default class InputFieldLayoutDefault extends Component<InputFieldLayoutSignature> {
  private readonly inputClass: string = 'input-field__input';

  private get inputId() {
    return this.args.inputId ?? guidFor(this);
  }

  <template>
    <div
      class='input-field{{if @required " input-field--required"}}{{if
          @errors
          " input-field--has-errors"
        }}{{if @disabled " input-field--disabled"}}'
    >
      {{#if @label}}
        <label for={{this.inputId}} class='input-field__label'>
          {{@label}}
        </label>
      {{/if}}

      <div class='input-field__box'>
        {{#each @prefixes as |prefix|}}
          <span class='input-field__prefix'>
            {{prefix}}
          </span>
        {{/each}}

        <@inputComponent
          class={{this.inputClass}}
          id={{this.inputId}}
          ...attributes
        />

        {{#each @suffixes as |suffix|}}
          <span class='input-field__suffix'>
            {{suffix}}
          </span>
        {{/each}}
      </div>

      {{#if @helptext}}
        <div class='input-field__helptext'>
          {{@helptext}}
        </div>
      {{/if}}

      {{#if @errors}}
        <div class='input-field__errors'>
          {{#each @errors as |error|}}
            <div class='input-field__error'>{{error}}</div>
          {{/each}}
        </div>
      {{/if}}
    </div>
  </template>
}
