import Component from '@glimmer/component';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';
import InputTextareaComponent from '../input/textarea.gts';

export default class InputFieldTextareaComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputTextareaComponent}}
      @fieldType='textarea'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
