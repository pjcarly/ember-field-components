import Component from '@glimmer/component';
import InputNumberComponent, {
  type InputNumberOptions,
} from '../input/number.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';
import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information';

export default class InputFieldPriceComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputNumberOptions>;
  Element: HTMLInputElement;
}> {
  @service declare fieldInformation: FieldInformationService;

  get currency() {
    return this.args.model['currency'] &&
      typeof this.args.model['currency'] === 'string'
      ? this.args.model['currency']
      : this.fieldInformation.defaultCurrency;
  }

  get inputFieldArgs() {
    const options = {
      ...this.args,
    };

    if (this.currency) {
      if (options.suffix) {
        if (Array.isArray(options.suffix)) {
          options.suffix = [this.currency, ...options.suffix];
        } else {
          options.suffix = [this.currency, options.suffix];
        }
      } else {
        options.suffix = this.currency;
      }
    }

    return options;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputNumberComponent}}
      @fieldType='price'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
