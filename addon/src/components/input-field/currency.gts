import Component from '@glimmer/component';
import InputSelectComponent from '../input/select.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';
import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information';
import type { IntlService } from 'ember-intl';

export default class InputFieldCurrencyComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  get selectOptions() {
    return this.fieldInformation.availableCurrencies.map((currency) => {
      return {
        value: currency,
        label: currency,
      };
    });
  }

  get inputFieldArgs() {
    const args = {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        selectOptions: this.selectOptions,
      },
    };

    return args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{InputSelectComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
