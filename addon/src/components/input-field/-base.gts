import Component from '@glimmer/component';
import type { ComponentLike } from '@glint/template';
import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information.ts';
import type FieldComponentsService from '../../services/field-components.ts';
import type { SomeInputFieldLayout } from './-layouts/index.gts';
import type { FieldOf, SomeModel } from '../../index.ts';
import type { SomeInputArguments } from '../input/-base.ts';
import {
  BaseFieldOptionsKeys,
  type BaseFieldOptions,
} from '../../decorators/field.ts';
import { cached } from '@glimmer/tracking';
import { get, set } from '@ember/object';
import { errorsFor } from '../field-messages.gts';

export const BaseInputFieldOptionsKeys = [
  ...BaseFieldOptionsKeys,
  'inputOptions',
  'inputId',
  'readonly',
  'required',
  'conditionalRequired',
];

export const filterBaseInputFieldOptions = <
  IO extends object = Record<never, never>,
>(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  options: Record<string, any>,
): BaseInputFieldOptions<IO> => {
  return Object.entries(options).reduce(
    (filteredOptions, [key, value]) => {
      if (BaseInputFieldOptionsKeys.includes(key)) {
        filteredOptions[key] = value;
      }

      return filteredOptions;
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    {} as Record<string, any>,
  );
};

/**
 * Options for the InputField that can also be passed using the field() decorator
 */
export interface BaseInputFieldOptions<IO extends object = Record<never, never>>
  extends BaseFieldOptions {
  /**
   * Options to be passed to the Input component.
   */
  inputOptions?: IO;
  /**
   * ID to be used for the Input component (and associated label).
   */
  inputId?: string;
  /**
   * Whether the input is readonly (disabled).
   */
  readonly?: boolean;
  /**
   * Whether the input is required
   */
  required?: boolean;
  /**
   * If this property / one of these properties is truthy, the input is also required
   */
  conditionalRequired?: string | string[];
}

/**
 * Options for an InputField* that can by passed as arguments
 */
export interface InputFieldArguments<
  O extends SomeModel,
  F extends FieldOf<O>,
  IO extends object = Record<never, never>,
> extends BaseInputFieldOptions<IO> {
  // Model / field / value
  /**
   * The model you want to render a field of.
   */
  model: O;
  /**
   * Key of the model's field you want to render.
   */
  field: F;
  /**
   * Whether the input should be disabled.
   */
  disabled?: boolean;
  /**
   * Optional callback when the value has changed
   */
  valueChanged?:
    | ((newValue: O[F], oldValue?: O[F]) => void)
    | ((newValue: O[F] | undefined, oldValue?: O[F] | undefined) => void);
  /**
   * Optional transform to run before setting the new value.
   */
  preSetHook?: (newValue: O[F]) => O[F];
  /**
   * Inline: whether to render only the input
   */
  inline?: boolean;
  /**
   * Optional: overwrite the default layout lookup mechanism by passing your own.
   */
  layout?: SomeInputFieldLayout;
  /**
   * Whether to show errors or not, defaults to true.
   * NOTE: Only works for Ember Data models
   */
  showErrors?: boolean;
  /**
   * Optional: helptext to render. If not defined, a translation will be sought.
   */
  helptext?: string;
}

export default class InputFieldTemplate<
  O extends SomeModel,
  F extends FieldOf<O>,
  IO extends object,
  I extends ComponentLike<{
    Args: SomeInputArguments<IO>;
    Element: HTMLElement;
  }>,
> extends Component<{
  Args: {
    inputFieldArgs: InputFieldArguments<O, F, IO>;
    inputComponent: I;
    fieldType: string;
    // layout?: SomeInputFieldLayout;
  };
  Element: HTMLElement;
}> {
  @service declare fieldInformation: FieldInformationService;
  @service declare fieldComponents: FieldComponentsService;

  private get value() {
    const { model, field } = this.args.inputFieldArgs;
    return get(model, field);
  }

  @cached
  get canBeSet() {
    const { model, field } = this.args.inputFieldArgs;

    let modelOrPrototype = model;
    // to be safe: only loop a maximum of 10 times
    let i = 0;

    do {
      const propertyDescriptor = Object.getOwnPropertyDescriptor(
        modelOrPrototype,
        field,
      );

      if (propertyDescriptor) {
        return propertyDescriptor.writable || !!propertyDescriptor.set;
      }

      modelOrPrototype = Object.getPrototypeOf(modelOrPrototype);
      i++;
    } while (modelOrPrototype && i <= 10);

    return false;
  }

  private valueChanged = (newValue: O[F]) => {
    const oldValue = this.value;

    if (!this.canBeSet) {
      return;
    }

    if (newValue !== undefined && this.args.inputFieldArgs.preSetHook) {
      newValue = this.args.inputFieldArgs.preSetHook(newValue);
    }

    set(
      this.args.inputFieldArgs.model,
      this.args.inputFieldArgs.field,
      newValue,
    );
    this.args.inputFieldArgs.valueChanged?.(newValue, oldValue);
  };

  private get layoutComponent() {
    return (
      // this.args.layout ??
      this.args.inputFieldArgs.layout ??
      this.fieldComponents.inputFieldLayoutFor(this.args.fieldType)
    );
  }

  private get helptext() {
    if (this.args.inputFieldArgs.helptext) {
      return this.args.inputFieldArgs.helptext;
    }

    return this.fieldInformation.getTranslatedFieldHelptext(
      this.args.inputFieldArgs.model,
      this.args.inputFieldArgs.field,
    );
  }

  private get errors() {
    const showErrors =
      this.args.inputFieldArgs.showErrors === undefined
        ? true
        : this.args.inputFieldArgs.showErrors;

    if (showErrors) {
      const { model, field } = this.args.inputFieldArgs;
      return errorsFor(model, field);
    }

    return undefined;
  }

  private get label(): string {
    const computedLabel = this.fieldInformation.getTranslatedFieldLabel(
      this.args.inputFieldArgs.model,
      this.args.inputFieldArgs.field,
    );
    return this.args.inputFieldArgs.label ?? computedLabel;
  }

  private get prefixes() {
    if (!this.args.inputFieldArgs.prefix) {
      return [];
    }

    return Array.isArray(this.args.inputFieldArgs.prefix)
      ? this.args.inputFieldArgs.prefix
      : [this.args.inputFieldArgs.prefix];
  }

  private get suffixes() {
    if (!this.args.inputFieldArgs.suffix) {
      return [];
    }

    return Array.isArray(this.args.inputFieldArgs.suffix)
      ? this.args.inputFieldArgs.suffix
      : [this.args.inputFieldArgs.suffix];
  }

  get disabled() {
    return !this.canBeSet || this.args.inputFieldArgs.disabled;
  }

  <template>
    {{#let
      (component
        @inputComponent
        value=this.value
        valueChanged=this.valueChanged
        options=@inputFieldArgs.inputOptions
        disabled=this.disabled
        required=@inputFieldArgs.required
      )
      as |BoundInputComponent|
    }}
      {{#if @inputFieldArgs.inline}}
        <BoundInputComponent
          id={{@inputFieldArgs.inputId}}
          {{! @glint-ignore ...attributes type is wrong }}
          ...attributes
        />
      {{else}}
        <this.layoutComponent
          @inputComponent={{BoundInputComponent}}
          @inputId={{@inputFieldArgs.inputId}}
          @disabled={{this.disabled}}
          @required={{@inputFieldArgs.required}}
          @label={{this.label}}
          @prefixes={{this.prefixes}}
          @suffixes={{this.suffixes}}
          @helptext={{this.helptext}}
          @errors={{this.errors}}
          {{! @glint-ignore ...attributes type is wrong }}
          ...attributes
        />
      {{/if}}
    {{/let}}
  </template>
}
