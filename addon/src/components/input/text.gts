import Component from '@glimmer/component';
import type { BaseInputArguments } from './-base';
import { on } from '@ember/modifier';

export interface InputTextArguments extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (newValue: string) => void;
}

export interface InputTextSignature {
  Args: InputTextArguments;
  Element: HTMLInputElement;
}

export default class InputTextComponent extends Component<InputTextSignature> {
  valueChanged = (ev: Event) => {
    if (this.args.disabled) {
      return;
    }

    if (ev.currentTarget && 'value' in ev.currentTarget) {
      const newValue = ev.currentTarget.value as string;
      this.args.valueChanged?.(newValue);
    }
  };

  <template>
    <input
      type='text'
      class='input input-text'
      ...attributes
      value={{@value}}
      disabled={{@disabled}}
      required={{@required}}
      {{on 'input' this.valueChanged}}
    />
  </template>
}
