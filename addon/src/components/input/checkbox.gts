import Component from '@glimmer/component';
import type { BaseInputArguments } from './-base.ts';
import { on } from '@ember/modifier';

export interface InputCheckboxArguments extends BaseInputArguments {
  value: boolean | undefined;
  valueChanged?: (newValue: boolean) => void;
}

export interface InputCheckboxSignature {
  Args: InputCheckboxArguments;
  Element: HTMLInputElement;
}

export default class InputCheckboxComponent extends Component<InputCheckboxSignature> {
  get disabled() {
    return !!this.args.disabled;
  }

  valueChanged = (ev: Event) => {
    if (this.disabled) {
      return;
    }

    if (ev.currentTarget && 'value' in ev.currentTarget) {
      const newValue = (ev.currentTarget as HTMLInputElement).checked;
      this.args.valueChanged?.(newValue);
    }
  };

  <template>
    <input
      type='checkbox'
      class='input input-checkbox'
      ...attributes
      checked={{@value}}
      disabled={{this.disabled}}
      required={{@required}}
      {{on 'input' this.valueChanged}}
    />
  </template>
}
