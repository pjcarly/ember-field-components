import Component from '@glimmer/component';
import type { BaseInputArguments } from './-base';
import { isBlank } from '@ember/utils';
import { on } from '@ember/modifier';
import { modifier } from 'ember-modifier';

export interface InputRangeArguments extends BaseInputArguments {
  value: number | undefined;
  valueChanged?: (newValue: number | undefined) => void;
}

export interface InputRangeSignature {
  Args: InputRangeArguments;
  Element: HTMLInputElement;
}

export default class InputRangeComponent extends Component<InputRangeSignature> {
  get disabled() {
    return !!this.args.disabled;
  }

  get isValidValue() {
    return this.args.value === undefined || typeof this.args.value === 'number';
  }

  private parse = (stringValue: string) => {
    // Normally, input range will not output empty strings
    if (!stringValue || isBlank(stringValue)) {
      return undefined;
    }

    let numberValue = parseFloat(stringValue);

    // Normally, input range will not output non-number strings
    if (isNaN(numberValue)) {
      // Not a number
      return undefined;
    }

    return numberValue;
  };

  valueChanged = (ev: Event) => {
    if (this.disabled) {
      return;
    }

    if (ev.currentTarget && 'value' in ev.currentTarget) {
      // Allow the input of just a minus sign, because a number might follow.
      if ((ev as InputEvent).data === '-' && ev.currentTarget.value === '') {
        return;
      }

      const newValue = this.parse(ev.currentTarget.value as string);
      this.args.valueChanged?.(newValue);

      if (newValue === undefined) {
        ev.currentTarget.value = '';
      }
    }
  };

  inputFocussed = () => {
    if (!this.isValidValue) {
      this.args.valueChanged?.(undefined);
    }
  };

  inputRangeModifier = modifier((element: HTMLInputElement) => {
    // Make sure element.value and this.args.value are "the same"
    const numberValue = isBlank(element.value)
      ? undefined
      : parseFloat(element.value);

    if (!this.isValidValue || this.args.value === undefined) {
      element.value = element.min === '' ? '0' : element.min;
    } else if (numberValue !== this.args.value) {
      element.value = this.args.value?.toString() ?? '';
    }
  });

  <template>
    <input
      type='range'
      class='input input-range{{unless
          this.isValidValue
          " input--invalid invalid"
        }}'
      ...attributes
      disabled={{this.disabled}}
      required={{@required}}
      {{on 'input' this.valueChanged}}
      {{on 'focus' this.inputFocussed}}
      {{this.inputRangeModifier}}
    />
  </template>
}
