import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { isBlank } from '@ember/utils';
import not from 'ember-truth-helpers/helpers/not';
import { on } from '@ember/modifier';
import Component from '@glimmer/component';
import { isGroup } from '../../interfaces/select-option-group.ts';
import type SelectOption from '../../interfaces/select-option.ts';
import type SelectOptionGroup from '../../interfaces/select-option-group.ts';
import type { BaseInputArguments } from './-base.ts';

export interface InputSelectOptions {
  selectOptions: (SelectOption | SelectOptionGroup)[];
  placeholder?: string;
}

export interface InputSelectArguments<
  Required extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (
    newValue: Required extends true ? string : string | undefined,
  ) => void;
  required?: Required;
  options: InputSelectOptions;
}

export interface InputSelectSignature<Required extends boolean | undefined> {
  Args: InputSelectArguments<Required>;
  Element: HTMLSelectElement;
}

export default class InputSelectComponent<
  Required extends boolean | undefined,
> extends Component<InputSelectSignature<Required>> {
  @service declare intl: IntlService;

  get showNone() {
    return isBlank(this.args.value) || !this.args.required;
  }

  get noneLabel() {
    return this.args.options?.placeholder ?? this.intl.t('input.select.none');
  }

  isSelected = (value: string) => {
    return this.args.value == value;
  };

  get isValidValue() {
    if (!this.args.value || isBlank(this.args.value)) {
      return true;
    }

    return this.valueInOptions(this.args.value);
  }

  valueInOptions = (value: string) => {
    return this.args.options.selectOptions.some((selectOption) => {
      if (isGroup(selectOption)) {
        return selectOption.selectOptions.some(
          (groupedOption) => groupedOption.value == value,
        );
      } else {
        return selectOption.value == value;
      }
    });
  };

  valueChanged = (ev: Event) => {
    if (this.args.disabled) {
      return;
    }

    if (ev.currentTarget && 'value' in ev.currentTarget) {
      const newValue = ev.currentTarget.value as string;

      if (isBlank(newValue) && !this.args.required) {
        // @ts-expect-error typing of Required messes up this piece of code
        this.args.valueChanged?.(undefined);
      } else if (this.valueInOptions(newValue)) {
        this.args.valueChanged?.(newValue);
      }
    }
  };

  formatInvalid(value?: string) {
    return `(?) ${value}`;
  }

  <template>
    <select
      class='input input-select{{unless this.isValidValue " input--invalid"}}'
      ...attributes
      {{on 'input' this.valueChanged}}
      required={{@required}}
      disabled={{@disabled}}
    >
      {{#if this.showNone}}
        <option selected={{not @value}} value='' disabled={{@required}}>
          {{this.noneLabel}}
        </option>
      {{/if}}
      {{#each @options.selectOptions as |selectOption|}}
        {{#if (isGroup selectOption)}}
          <optgroup label={{selectOption.label}}>
            {{#each selectOption.selectOptions as |groupedSelectOption|}}
              <option
                value={{groupedSelectOption.value}}
                selected={{this.isSelected groupedSelectOption.value}}
              >{{if
                  groupedSelectOption.label
                  groupedSelectOption.label
                  groupedSelectOption.value
                }}</option>
            {{/each}}
          </optgroup>
        {{else}}
          <option
            value={{selectOption.value}}
            selected={{this.isSelected selectOption.value}}
          >{{if
              selectOption.label
              selectOption.label
              selectOption.value
            }}</option>
        {{/if}}
      {{/each}}
      {{#unless this.isValidValue}}
        <option value={{@value}} selected={{true}} disabled={{true}}>
          {{this.formatInvalid @value}}
        </option>
      {{/unless}}
    </select>
  </template>
}
