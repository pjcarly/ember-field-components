import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldTextareaComponent from '../components/input-field/textarea.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldTextComponent from '../components/output-field/text.gts';
import type { OutputTextOptions } from '../components/output/text.ts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

type FieldTextareaOptions = BaseInputFieldOptions;

const textarea = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldTextareaOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions: BaseOutputFieldOptions<OutputTextOptions> = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      multiLine: true,
    },
  };

  return {
    type: 'textarea',
    inputField: InputFieldTextareaComponent,
    inputFieldOptions,
    outputField: OutputFieldTextComponent,
    outputFieldOptions,
  };
};

export default textarea;
