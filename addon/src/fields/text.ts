import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldTextComponent from '../components/input-field/text.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldTextComponent from '../components/output-field/text.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

type FieldTextOptions = BaseInputFieldOptions;

const text = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldTextOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'text',
    inputField: InputFieldTextComponent,
    inputFieldOptions,
    outputField: OutputFieldTextComponent,
    outputFieldOptions,
  };
};

export default text;
