import type { FieldOf, SomeModel } from '..';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldSwitchComponent from '../components/input-field/switch.gts';
import type { FieldDecoratorOptions } from '../decorators/field';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldSwitchComponent from '../components/output-field/switch.gts';

type FieldCheckboxSwitchOptions = BaseInputFieldOptions;

const checkboxSwitch = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldCheckboxSwitchOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'checkbox-switch',
    inputField: InputFieldSwitchComponent,
    inputFieldOptions,
    outputField: OutputFieldSwitchComponent,
    outputFieldOptions,
  };
};

export default checkboxSwitch;
