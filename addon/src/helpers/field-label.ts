import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import type FieldInformationService from '../services/field-information';
import type { FieldOf, SomeModel } from '..';

type PositionalModelName = [modelName: string, field: string];
type Positional<O extends SomeModel, F extends FieldOf<O>> = [
  model: O,
  field: F,
];

export interface FieldLabelHelperSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: {
    Positional: PositionalModelName | Positional<O, F>;
  };
  Return: string;
}

export default class FieldLabelHelper<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Helper<FieldLabelHelperSignature<O, F>> {
  @service declare fieldInformation: FieldInformationService;

  compute([modelOrModelName, field]: PositionalModelName | Positional<O, F>) {
    return this.fieldInformation.getTranslatedFieldLabel(
      // @ts-expect-error can't get the types right
      modelOrModelName,
      field,
    );
  }
}
