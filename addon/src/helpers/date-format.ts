import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import FieldInformationService from '../services/field-information.ts';
import { format, isValid } from 'date-fns';

type Positional = [value: Date | undefined | null, dateFormat?: string];

interface DateFormatSignature {
  Args: {
    Positional: Positional;
  };
  Return: string | undefined;
}

export default class DateFormatHelper extends Helper<DateFormatSignature> {
  @service declare fieldInformation: FieldInformationService;

  compute([value, dateFormat]: Positional) {
    if (!value || isBlank(value) || !isValid(value)) {
      return undefined;
    }

    if (!dateFormat || isBlank(dateFormat)) {
      return format(value, this.fieldInformation.dateFormat);
    } else {
      return format(value, dateFormat);
    }
  }
}
