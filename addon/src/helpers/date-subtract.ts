import { isBlank } from '@ember/utils';
import { type Duration } from 'date-fns';
import { sub } from 'date-fns';

export default function dateSubtract(
  value: Date | undefined | null,
  duration: Duration,
) {
  if (!value || isBlank(value)) {
    return undefined;
  }

  return sub(value, duration);
}
