import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import type FieldInformationService from '../services/field-information';
import type { FieldOf, SomeModel } from '..';

type PositionalModelName = [modelName: string, field: string];
type Positional<O extends SomeModel, F extends FieldOf<O>> = [
  model: O,
  field: F,
];

export interface FieldHelptextHelperSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: {
    Positional: PositionalModelName | Positional<O, F>;
  };
  Return: string | undefined;
}

export default class FieldHelptextHelper<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Helper<FieldHelptextHelperSignature<O, F>> {
  @service declare fieldInformation: FieldInformationService;

  compute([modelOrModelName, field]: PositionalModelName | Positional<O, F>) {
    return this.fieldInformation.getTranslatedFieldHelptext(
      // @ts-expect-error can't get the types right
      modelOrModelName,
      field,
    );
  }
}
