// Add any types here that you need for local development only.
// These will *not* be published as part of your addon, so be careful that your published code does not rely on them!

/**
 * TYPES
 */
import 'ember-cached-decorator-polyfill';

/**
 * GLINT
 */
import '@glint/environment-ember-loose';
import '@glint/environment-ember-template-imports';

import type EmberTruthRegistry from 'ember-truth-helpers/template-registry';
import type EmberIntlRegistry from 'ember-intl/template-registry';
import type FieldComponentsRegistry from '../src/template-registry';

declare module '@glint/environment-ember-loose/registry' {
  export default interface Registry
    extends FieldComponentsRegistry,
      EmberTruthRegistry,
      EmberIntlRegistry {
    // local entries
  }
}
