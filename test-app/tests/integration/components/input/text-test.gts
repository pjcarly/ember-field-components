import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { fillIn, render, settled } from '@ember/test-helpers';
import InputTextComponent from '@getflights/ember-field-components/components/input/text';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | input/text', function (hooks) {
  setupRenderingTest(hooks);

  test('Input::Text', async function (assert) {
    const state = tracked<{
      value: string | undefined;
      disabled: boolean;
    }>({
      value: undefined,
      disabled: true,
    });

    const valueChanged = (newValue: string) => {
      state.value = newValue;
    };

    await render(<template>
      <InputTextComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');

    await fillIn('[data-test-input]', 'Some text');

    assert
      .dom('[data-test-input]')
      .hasValue('Some text', 'Value is "Some text" after filling in.');
    assert.strictEqual(
      state.value,
      'Some text',
      'Variable has been updated by valueChanged',
    );

    state.value = 'Other text';
    await settled();
    assert
      .dom('[data-test-input]')
      .hasValue('Other text', 'Value is "Other text" after updating @value.');
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputTextComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
