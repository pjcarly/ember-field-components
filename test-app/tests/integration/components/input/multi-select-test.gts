import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, select, settled } from '@ember/test-helpers';
import InputMultiSelectComponent from '@getflights/ember-field-components/components/input/multi-select';
import { tracked } from 'tracked-built-ins';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | input/multi-select', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Input::MultiSelect', async function (assert) {
    const selectOptions: (SelectOption | SelectOptionGroup)[] = [
      {
        label: 'Option 1',
        value: 'option1',
      },
      {
        label: 'Group',
        selectOptions: [
          {
            label: 'Option 2',
            value: 'option2',
          },
        ],
      },
      {
        label: 'Option 3',
        value: 'option3',
      },
    ];

    const state = tracked({
      value: undefined as string[] | undefined,
    });

    const valueChanged = (newValue: string[] | undefined) => {
      state.value = newValue;
    };

    const options = { selectOptions };

    await render(<template>
      <InputMultiSelectComponent
        @options={{options}}
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'size',
        '5',
        'Size is 5 (none + opt 1 + group + opt 2 + opt3).',
      );
    assert
      .dom('[data-test-input] option')
      .exists({ count: 4 }, 'There are 4 options (none + 3 options)');

    assert
      .dom('[data-test-input] > option[value=""]')
      .exists('There is an option for none.');
    assert.dom('[data-test-input] > option[value=""]').hasText('-- None --');

    assert
      .dom('[data-test-input] > option[value=""]')
      .isEnabled('The none option is clickable.');
    assert
      .dom('[data-test-input] > option[value=""]')
      .matchesSelector(':checked', 'The none option is selected.');

    assert
      .dom('[data-test-input] > option[value="option1"]')
      .exists('There is an option for option1.');
    assert
      .dom('[data-test-input] > option[value="option1"]')
      .hasText('Option 1', 'option1 has correct label "Option 1"');

    assert
      .dom('[data-test-input] > optgroup[label="Group"]')
      .exists('There is an option group "Group".');

    assert
      .dom(
        '[data-test-input] > optgroup[label="Group"] > option[value="option2"]',
      )
      .exists('There is an option for option2 in option group "Group".');
    assert
      .dom(
        '[data-test-input] > optgroup[label="Group"] > option[value="option2"]',
      )
      .hasText('Option 2', 'option2 has correct label "Option 2"');

    assert
      .dom('[data-test-input] > option[value="option3"]')
      .exists('There is an option for option3.');
    assert
      .dom('[data-test-input] > option[value="option3"]')
      .hasText('Option 3', 'option3 has correct label "Option 3"');

    await select('[data-test-input]', ['option2', 'option3']);

    assert
      .dom('[data-test-input] option[value="option2"]')
      .matchesSelector(':checked', 'option2 is selected');
    assert
      .dom('[data-test-input] option[value="option3"]')
      .matchesSelector(':checked', 'option3 is also selected');
    assert.propEqual(
      state.value,
      ['option2', 'option3'],
      'Variable has been updated by valueChanged',
    );

    state.value = ['option2'];
    await settled();
    assert
      .dom('[data-test-input]')
      .hasValue('option2', 'Value is "option2" after updating @value.');
    assert
      .dom('[data-test-input] option[value="option2"]')
      .matchesSelector(':checked', 'option2 is selected');

    await select('[data-test-input]', '');
    assert
      .dom('[data-test-input]')
      .hasValue('', 'Value is undefined after selecting None');
    assert
      .dom('[data-test-input] option[value=""]')
      .matchesSelector(':checked', 'None is selected');
    assert.propEqual(
      state.value,
      [],
      'Variable has been updated by valueChanged',
    );
  });

  test('@required', async function (assert) {
    const selectOptions: (SelectOption | SelectOptionGroup)[] = [
      {
        label: 'Option 1',
        value: 'option1',
      },
      {
        label: 'Option 2',
        value: 'option2',
      },
    ];

    const state = tracked<{
      value: string[] | undefined;
      required?: boolean;
    }>({
      value: undefined as string[] | undefined,
      required: true,
    });

    const options = { selectOptions } as const;

    const valueChanged = (newValue: string[]) => {
      state.value = newValue;
    };

    await render(<template>
      <InputMultiSelectComponent
        @options={{options}}
        @value={{state.value}}
        @required={{true}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');
    assert
      .dom('[data-test-input] option')
      .exists({ count: 3 }, 'There are 3 options (none + 2 options)');

    assert
      .dom('[data-test-input] > option[value=""]')
      .exists('There is an option for none.');
    assert.dom('[data-test-input] > option[value=""]').hasText('-- None --');
    assert
      .dom('[data-test-input] > option[value=""]')
      .isDisabled('The none option is not clickable.');
    assert
      .dom('[data-test-input] > option[value=""]')
      .matchesSelector(':checked', 'The none option is selected.');

    assert
      .dom('[data-test-input] > option[value="option1"]')
      .exists('There is an option for option1.');
    assert
      .dom('[data-test-input] > option[value="option1"]')
      .hasText('Option 1', 'option1 has correct label "Option 1"');

    assert
      .dom('[data-test-input] > option[value="option2"]')
      .exists('There is an option for option2 in option group "Group".');
    assert
      .dom('[data-test-input] > option[value="option2"]')
      .hasText('Option 2', 'option2 has correct label "Option 2"');

    await select('[data-test-input]', ['option1', 'option2']);

    assert
      .dom('[data-test-input] option')
      .exists({ count: 2 }, 'There are 2 options (2 options without none)');

    assert
      .dom('[data-test-input] option[value="option1"]')
      .matchesSelector(':checked', 'option1 is selected');
    assert
      .dom('[data-test-input] option[value="option2"]')
      .matchesSelector(':checked', 'option2 is also selected');
    assert.propEqual(
      state.value,
      ['option1', 'option2'],
      'Variable has been updated by valueChanged',
    );

    assert
      .dom('[data-test-input] > option[value=""]')
      .doesNotExist('The none option does not exist anymore.');
  });

  test('Invalid value', async function (assert) {
    const selectOptions: (SelectOption | SelectOptionGroup)[] = [
      {
        label: 'Option 1',
        value: 'option1',
      },
    ];

    const state = tracked({
      value: ['Not an option'] as string[] | undefined,
    });

    const valueChanged = (newValue: string[] | undefined) => {
      state.value = newValue;
    };

    const options = { selectOptions };

    await render(<template>
      <InputMultiSelectComponent
        @options={{options}}
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .hasValue('Not an option', 'Value is "Not an option" at first.');
    assert
      .dom('[data-test-input]')
      .hasClass('input--invalid', 'Input has invalid class');
    assert
      .dom('[data-test-input] option')
      .exists({ count: 3 }, 'There are 3 options (none + 1 option + invalid)');

    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .exists('There is an option for "Not an option"');
    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .hasText('(?) Not an option', 'Option has text "(?) Not an option"');
    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .matchesSelector(':checked', '"Not an option" is selected');
    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .isDisabled('"Not an option" is disabled');

    assert
      .dom('[data-test-input] > option[value=""]')
      .exists('There is an option for none.');
    assert
      .dom('[data-test-input] > option[value="option1"]')
      .exists('There is an option for option1.');

    state.value = ['option1', 'Not an option'];
    await settled();

    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .matchesSelector(
        ':checked',
        '"Not an option" is selected after updating @value',
      );
    assert
      .dom('[data-test-input] > option[value="option1"]')
      .matchesSelector(
        ':checked',
        'option1 is also selected after updating @value',
      );

    await select('[data-test-input]', ['option1']);

    assert
      .dom('[data-test-input]')
      .doesNotHaveClass('input--invalid', 'Input does not have invalid class');
    assert
      .dom('[data-test-input] option')
      .exists({ count: 2 }, 'There are 2 options (none + 1 option)');

    assert
      .dom('[data-test-input]')
      .hasValue('option1', 'Value is "option1" after selecting.');
    assert
      .dom('[data-test-input] option[value="option1"]')
      .matchesSelector(':checked', 'option1 is selected');
    assert.propEqual(
      state.value,
      ['option1'],
      'Variable has been updated by valueChanged',
    );

    assert
      .dom('[data-test-input] > option[value="Not an option"]')
      .doesNotExist('The option for "Not an option" is gone');
  });
});
