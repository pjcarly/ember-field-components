import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled, focus, fillIn } from '@ember/test-helpers';
import InputRangeComponent from '@getflights/ember-field-components/components/input/range';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | input/range', function (hooks) {
  setupRenderingTest(hooks);

  test('Input::Range', async function (assert) {
    const state = tracked<{
      value?: number;
      min?: number;
    }>({
      value: undefined,
      min: undefined,
    });

    const valueChanged = (newValue: number | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputRangeComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        min={{state.min}}
        data-test-input
      />
    </template>);

    /**
     * 1. Only numeric values allowed. Undefined or non-text values are rendered as the minimum (defaults to "0").
     */

    assert
      .dom('[data-test-input]')
      .hasValue('0', 'Value is min (0) at first, because value is undefined.');

    // Text values being passed to @value are rendered as the minimum value (default '0')
    // @ts-expect-error not a number
    state.value = 'Some text';
    await settled();

    assert
      .dom('[data-test-input]')
      .hasValue(
        '0',
        'Input has value of min (0) because @value is not a number',
      );

    await focus('[data-test-input]');
    assert.strictEqual(
      state.value,
      undefined,
      '[ONLY WORKS WHEN BROWSER IS NOT FOCUSSED] Variable has been updated to undefined by valueChanged (because text is cleared on a focus event).',
    );

    // Min is 10
    state.min = 10;
    // @ts-expect-error not a number
    state.value = 'Some text';
    await settled();

    assert
      .dom('[data-test-input]')
      .hasValue(
        '10',
        'Input has value of 10 because @value is not a number and the minimum is 10',
      );

    /**
     * 2. Inputting numbers works + they are correctly passed (as real numbers) to @valueChanged
     */
    await fillIn('[data-test-input]', '56');
    assert
      .dom('[data-test-input]')
      .hasValue('56', 'Value is "56" after filling in.');
    assert.strictEqual(
      state.value,
      56,
      'Variable has been updated by valueChanged.',
    );
    // It is actually a number
    assert.strictEqual(
      typeof state.value,
      'number',
      'Type of variable is a number, parsing ok.',
    );
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputRangeComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
