import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import OutputTextComponent from '@getflights/ember-field-components/components/output/text';
import { tracked } from 'tracked-built-ins';
import { hash } from '@ember/helper';

module('Integration | Component | output/text', function (hooks) {
  setupRenderingTest(hooks);

  test('Output::Text', async function (assert) {
    const state = tracked({
      value: undefined as string | undefined,
    });

    await render(<template>
      <OutputTextComponent @value={{state.value}} data-test-output />
    </template>);

    assert.dom('[data-test-output]').hasText('', 'Value is empty at first.');
    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Output has class "output--empty".');

    state.value = 'Some text';
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText('Some text', 'Value is "Some text" after updating @value.');
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass(
        'output--empty',
        'Output does not have class "output--empty".',
      );
  });

  test('Multiline', async function (assert) {
    const state = tracked({
      value: 'One liner' as string | undefined,
    });

    await render(<template>
      <OutputTextComponent
        @value={{state.value}}
        @options={{hash multiLine=true}}
        data-test-output
      />
    </template>);

    assert
      .dom('[data-test-output]')
      .hasText('One liner', 'Value is "One liner" at first.');
    assert.dom('[data-test-output] br').doesNotExist('No <br> elements');
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass('output--empty', 'Output has class "output--empty".');

    state.value = 'First line\nSecond line';
    await settled();
    assert
      .dom('[data-test-output]')
      .containsText(
        'First line',
        'Value has "First line" after updating @value.',
      );
    assert
      .dom('[data-test-output]')
      .containsText(
        'Second line',
        'Value has "Second line" after updating @value.',
      );
    assert.dom('[data-test-output] br').exists({ count: 1 }, '1 <br> element');

    state.value = 'First line\nSecond line\nThird line';
    await settled();
    assert
      .dom('[data-test-output]')
      .containsText(
        'First line',
        'Value has "First line" after updating @value (3 lines).',
      );
    assert
      .dom('[data-test-output]')
      .containsText(
        'Second line',
        'Value has "Second line" after updating @value (3 lines).',
      );
    assert
      .dom('[data-test-output]')
      .containsText(
        'Third line',
        'Value has "Third line" after updating @value (3 lines).',
      );
    assert
      .dom('[data-test-output] br')
      .exists({ count: 2 }, '2 <br> elements (3 lines)');
  });
});
