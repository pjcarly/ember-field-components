import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import OutputCheckboxComponent from '@getflights/ember-field-components/components/output/checkbox';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | output/checkbox', function (hooks) {
  setupRenderingTest(hooks);

  test('Output::Checkbox', async function (assert) {
    const state = tracked({
      value: undefined as boolean | undefined,
    });

    /* prettier-ignore */
    await render(<template>
      <OutputCheckboxComponent @value={{state.value}} data-test-output />
    </template>);

    assert
      .dom('[data-test-output] input[type="checkbox"]')
      .doesNotExist('No checkbox exists for undefined');

    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Has empty css class');

    state.value = true;
    await settled();

    assert
      .dom('[data-test-output] input[type="checkbox"]')
      .isChecked('Is checked for `true`.');

    state.value = false;
    await settled();

    assert
      .dom('[data-test-output] input[type="checkbox"]')
      .isNotChecked('Is not checked for `false`.');

    // @ts-expect-error string is not a boolean
    state.value = 'Not a boolean';
    await settled();

    assert
      .dom('[data-test-output] input[type="checkbox"]')
      .isChecked('Is checked when value is a non-boolean truthy value.');
  });
});
