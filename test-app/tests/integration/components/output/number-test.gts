import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import OutputNumberComponent from '@getflights/ember-field-components/components/output/number';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | output/number', function (hooks) {
  setupRenderingTest(hooks);

  test('Output::Number', async function (assert) {
    const state = tracked({
      value: undefined as number | undefined,
    });

    await render(<template>
      <OutputNumberComponent @value={{state.value}} data-test-output />
    </template>);

    assert.dom('[data-test-output]').hasText('', 'Value is empty at first.');
    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Output has class "output--empty".');

    state.value = 0;
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText('0', 'Value is "0" after updating @value.');
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass(
        'output--empty',
        'Output has does not have "output--empty".',
      );

    state.value = 123.45;
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText('123.45', 'Value is "123.45" after updating @value.');
  });
});
