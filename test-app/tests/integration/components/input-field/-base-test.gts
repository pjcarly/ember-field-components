import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import InputTextComponent from '@getflights/ember-field-components/components/input/text';
import { render } from '@ember/test-helpers';
import { tracked } from 'tracked-built-ins';
import { fillIn } from '@ember/test-helpers';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import { settled } from '@ember/test-helpers';
import type FieldComponentsService from '@getflights/ember-field-components/services/field-components';
import type { TOC } from '@ember/component/template-only';
import type { InputFieldLayoutSignature } from '@getflights/ember-field-components/components/input-field/-layouts/index';
import Model, { attr } from '@ember-data/model';
import type Store from '@ember-data/store';
import Component from '@glimmer/component';
import type { InputFieldArguments } from '@getflights/ember-field-components/components/input-field/-base';
import InputFieldTemplate from '@getflights/ember-field-components/components/input-field/-base';

/**
 * Create test input field layout
 */
const TestLayout: TOC<InputFieldLayoutSignature> = <template>
  <div data-test-required={{@required}} data-test-layout>
    {{#if @label}}
      <label data-test-label>
        {{@label}}
      </label>
    {{/if}}
    <div data-test-prefixes>
      {{#each @prefixes as |prefix|}}
        <span data-test-prefix>
          {{prefix}}
        </span>
      {{/each}}
    </div>
    <@inputComponent ...attributes data-test-input />
    <div data-test-suffixes>
      {{#each @suffixes as |suffix|}}
        <span data-test-suffix>
          {{suffix}}
        </span>
      {{/each}}
    </div>
    <div data-test-errors>
      {{#each @errors as |error|}}
        <div data-test-error>{{error}}</div>
      {{/each}}
    </div>
  </div>
</template>;

/**
 * Create test input field
 */

class InputFieldTestComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputTextComponent}}
      @fieldType="test"
    />
  </template>
}

module('Integration | Component | input-field/-base', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  hooks.beforeEach(function () {
    const fieldComponentsService = this.owner.lookup(
      'service:field-components',
    ) as FieldComponentsService;

    /**
     * Create test layout
     */
    fieldComponentsService.setDefaultInputFieldLayout(TestLayout);
  });

  test('@inline: layout or just the input', async function (assert) {
    const model = { field: 'initial' };
    const state = tracked<{ inline?: boolean }>({});

    await render(<template>
      <InputFieldTestComponent
        @model={{model}}
        @field={{'field'}}
        @inline={{state.inline}}
      />
    </template>);

    /**
     * Inline = undefined
     */
    // Rendered in a layout
    assert
      .dom('[data-test-layout]')
      .exists(
        '[undefined] Input Field is rendered with a layout + Layout resolution using field-components service works',
      );

    /**
     * Inline = true
     */
    state.inline = true;
    await settled();

    // Not rendered in a layout
    assert
      .dom('[data-test-layout]')
      .doesNotExist('[true] Input Field is rendered without a layout');
    // Only an input is rendered
    assert
      .dom('input')
      .exists('[true] Input Field has an input')
      .matchesSelector(
        ':nth-child(1):nth-last-child(1)',
        '[true] The input is the only element in the parent (it is the first and last child)',
      );

    /**
     * Inline = false
     */
    state.inline = false;
    await settled();

    // Rendered in a layout
    assert
      .dom('[data-test-layout]')
      .exists('[@inline = false] Input Field is rendered with a layout');
  });

  test('Two way binding, label, prefixes, suffixes', async function (assert) {
    const fieldInformationService = this.owner.lookup(
      'service:field-information',
    ) as FieldInformationService;

    class ModelClass {
      @tracked someField = 'initial';
    }

    const model = new ModelClass();

    const state = tracked<{
      prefix?: string | string[];
      suffix?: string | string[];
    }>({});

    await render(<template>
      <InputFieldTestComponent
        @model={{model}}
        @field='someField'
        @prefix={{state.prefix}}
        @suffix={{state.suffix}}
      />
    </template>);

    /**
     * Has "two way binding"
     */
    assert
      .dom('[data-test-input]')
      .hasValue(
        'initial',
        '[Value binding] Input has the correct value "initial',
      );

    await fillIn('[data-test-input]', 'newValue');

    assert
      .dom('[data-test-input]')
      .hasValue(
        'newValue',
        '[Value binding] Input has the new value "newValue"',
      );
    assert.strictEqual(
      model.someField,
      'newValue',
      '[Value binding] model.field has the new value "newValue"',
    );
    /**
     * Label
     */
    assert
      .dom('[data-test-label]')
      .hasText(
        fieldInformationService.getTranslatedFieldLabel(model, 'someField'),
        '[Label] Label is result from fieldInformationService.getTranslatedFieldLabel',
      );

    /**
     * Prefix(es)
     */
    // None
    assert
      .dom('[data-test-prefix]')
      .doesNotExist('[Prefix] undefined --> No prefixes rendered.');

    // One
    state.prefix = 'Prefix';
    await settled();
    assert
      .dom('[data-test-prefix]')
      .exists({ count: 1 }, '[Prefix] "Prefix" --> One prefix rendered.')
      .hasText('Prefix', '[Prefix] "Prefix" --> Prefix has text "Prefix"');

    // Array
    state.prefix = ['PrefixArray'];
    await settled();
    assert
      .dom('[data-test-prefix]')
      .exists({ count: 1 }, '[Prefix] ["PrefixArray"] --> One prefix rendered.')
      .hasText(
        'PrefixArray',
        '[Prefix] ["PrefixArray"] --> Prefix has text "PrefixArray"',
      );

    state.prefix = ['PrefixArray', 'SecondPrefix'];
    await settled();
    assert
      .dom('[data-test-prefix]')
      .exists(
        { count: 2 },
        '[Prefix] ["PrefixArray", "SecondPrefix"] --> Two prefixes rendered.',
      );

    assert
      .dom('[data-test-prefix]:nth-child(1)')
      .hasText(
        'PrefixArray',
        '[Prefix] ["PrefixArray", "SecondPrefix"] --> First prefix has text "PrefixArray"',
      );
    assert
      .dom('[data-test-prefix]:nth-child(2)')
      .hasText(
        'SecondPrefix',
        '[Prefix] ["PrefixArray", "SecondPrefix"] --> Second prefix has text "SecondPrefix"',
      );

    /**
     * Suffix(es)
     */
    // None
    assert
      .dom('[data-test-suffix]')
      .doesNotExist('[Suffix] undefined --> No suffixes rendered.');

    // One
    state.suffix = 'Suffix';
    await settled();
    assert
      .dom('[data-test-suffix]')
      .exists({ count: 1 }, '[Suffix] "Suffix" --> One suffix rendered.')
      .hasText('Suffix', '[Suffix] "Suffix" --> Suffix has text "Suffix"');

    // Array
    state.suffix = ['SuffixArray'];
    await settled();
    assert
      .dom('[data-test-suffix]')
      .exists({ count: 1 }, '[Suffix] ["SuffixArray"] --> One suffix rendered.')
      .hasText(
        'SuffixArray',
        '[Suffix] ["SuffixArray"] --> Suffix has text "SuffixArray"',
      );

    state.suffix = ['SuffixArray', 'SecondSuffix'];
    await settled();
    assert
      .dom('[data-test-suffix]')
      .exists(
        { count: 2 },
        '[Suffix] ["SuffixArray", "SecondSuffix"] --> Two suffixes rendered.',
      );

    assert
      .dom('[data-test-suffix]:nth-child(1)')
      .hasText(
        'SuffixArray',
        '[Suffix] ["SuffixArray", "SecondSuffix"] --> First suffix has text "SuffixArray"',
      );
    assert
      .dom('[data-test-suffix]:nth-child(2)')
      .hasText(
        'SecondSuffix',
        '[Suffix] ["SuffixArray", "SecondSuffix"] --> Second suffix has text "SecondSuffix"',
      );
  });

  test('Errors (only for Ember Data models)', async function (assert) {
    const store = this.owner.lookup('service:store') as Store;

    class SomeEDModel extends Model {
      @attr()
      declare someField: string;
    }

    this.owner.register('model:some-model', SomeEDModel);

    const model = store.createRecord('some-model') as SomeEDModel;

    const state = tracked<{
      showErrors?: boolean;
    }>({});

    await render(<template>
      <InputFieldTestComponent
        @model={{model}}
        @field='someField'
        @showErrors={{state.showErrors}}
      />
    </template>);

    /**
     * Errors are shown by default, but there are no errors yet
     */
    assert.dom('[data-test-error]').doesNotExist('No errors yet.');

    model.errors.add('someField', 'Some error');
    await settled();

    /**
     * Errors are shown by default, one error is set
     */
    assert
      .dom('[data-test-error]')
      .exists({ count: 1 }, 'Errors are shown by default. There is one error.')
      .hasText('Some error');

    model.errors.add('someField', 'Some second error');
    await settled();

    assert
      .dom('[data-test-error]')
      .exists(
        { count: 2 },
        'Errors are shown by default. There are two errors.',
      );

    /**
     * showErrors is false, no errors are shown
     */
    state.showErrors = false;
    await settled();

    assert
      .dom('[data-test-error]')
      .doesNotExist('If @showErrors is false, no errors are rendered.');
    /**
     * showErrors is true, errors are shown
     */
    state.showErrors = true;
    await settled();

    assert
      .dom('[data-test-error]')
      .exists('If @showErrors is true, errors are rendered.');
  });
});
