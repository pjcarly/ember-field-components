import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import InputFieldNumberComponent from '@getflights/ember-field-components/components/input-field/number';

module('Integration | Component | input-field/number', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('renders an input/number', async function (assert) {
    const model = { field: 'value' };

    await render(<template>
      <InputFieldNumberComponent @model={{model}} @field={{'field'}} />
    </template>);

    assert
      .dom('input')
      .hasClass('input-number', 'Input has "input-number" class.');
  });
});
