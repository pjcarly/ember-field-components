import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { setupIntl } from 'ember-intl/test-support';
import FieldLabelHelper from '@getflights/ember-field-components/helpers/field-label';
import Service from '@ember/service';

module('Integration | Helper | field-label', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test("Uses FieldInformationService's getTranslatedFieldLabel", async function (assert) {
    class FieldInformationStub extends Service {
      getTranslatedFieldLabel(model: any, field: string) {
        return `OK+${model.name}+${field}`;
      }
    }

    this.owner.register('service:field-information', FieldInformationStub);

    class SomeModel {
      name = 'SomeModel';
      someProperty = 'someValue';
    }

    const model = new SomeModel();

    await render(<template>
      {{! @glint-ignore it is complaining for some weird reason }}
      {{FieldLabelHelper model 'someProperty'}}
    </template>);

    assert
      .dom()
      .hasText(
        `OK+SomeModel+someProperty`,
        "Field label helper uses FieldInformationService's getTranslatedFieldLabel",
      );
  });
});
