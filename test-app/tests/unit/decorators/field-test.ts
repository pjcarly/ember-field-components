import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import field, {
  type FieldDecoratorOptions,
} from '@getflights/ember-field-components/decorators/field';
import InputFieldTextComponent from '@getflights/ember-field-components/components/input-field/text';
import type FieldComponentsService from '@getflights/ember-field-components/services/field-components';
import type { FieldOf } from '@getflights/ember-field-components';

module('Unit | Decorator | field', function (hooks) {
  setupRenderingTest(hooks);

  /**
   * The field decorator registers the optionFn
   */
  test('Decorator correctly sets optionsFn. Field information service passes owner, target, field to the OptionsFn when accessing getFieldOptions()', async function (assert) {
    const someField = <
      O extends SomeModel,
      F extends FieldOf<O>,
    >(): FieldDecoratorOptions<O, F> => {
      return {
        type: 'someField',
        inputField: InputFieldTextComponent,
        inputFieldOptions: {},
      };
    };

    class SomeModel {
      @field(someField())
      someProperty: unknown;
    }

    const model = new SomeModel();

    const fieldComponentsService = this.owner.lookup(
      'service:field-components',
    ) as FieldComponentsService;

    const resolvedOptions = fieldComponentsService.getFieldOptions(
      model,
      'someProperty',
    );

    const expectedOptions = someField();

    assert.propEqual(
      {
        inputField: resolvedOptions?.inputField,
        inputFieldOptions: resolvedOptions?.inputFieldOptions,
      },
      {
        inputField: expectedOptions.inputField,
        inputFieldOptions: expectedOptions.inputFieldOptions,
      },
      'Options are correctly set by the field decorator and retrieve by the fieldComponentsService.',
    );
  });
});
