import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import Store from '@ember-data/store';
import { addTranslations, setupIntl } from 'ember-intl/test-support';
import Model from '@ember-data/model';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';

module('Unit | Service | field-information', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Check if the label is the correct translation', async function (assert) {
    // No model, no global translation: turn camelCase into Camel Text
    class NotAnEDModel {
      someProperty = 'someValue';
    }

    const fieldInformationService = this.owner.lookup(
      'service:field-information',
    ) as FieldInformationService;
    const store = this.owner.lookup('service:store') as Store;

    const model = new NotAnEDModel();

    assert.strictEqual(
      fieldInformationService.getTranslatedFieldLabel(model, 'someProperty'),
      'Some Property',
      'No model, no global translation: turn camelCase into Camel Text',
    );

    // No model, has a global translation, uses this one
    addTranslations('en-001', {
      'ember-field-components': {
        global: {
          fields: {
            someProperty: 'Some Translation',
          },
        },
      },
    });

    assert.strictEqual(
      fieldInformationService.getTranslatedFieldLabel(model, 'someProperty'),
      'Some Translation',
      'No model, has a global translation, uses this one',
    );

    // Ember Data model
    class SomeEDModel extends Model {
      someProperty = 'someValue';
    }

    this.owner.register('model:some-model', SomeEDModel);

    const EDModel = store.createRecord('some-model') as SomeEDModel;

    assert.strictEqual(
      fieldInformationService.getTranslatedFieldLabel(EDModel, 'someProperty'),
      'Some Translation',
      'Ember Data Model: no translation for model name, uses global translation',
    );

    addTranslations('en-001', {
      'ember-field-components': {
        'some-model': {
          fields: {
            someProperty: 'Some Model Translation',
          },
        },
      },
    });

    assert.strictEqual(
      fieldInformationService.getTranslatedFieldLabel(EDModel, 'someProperty'),
      'Some Model Translation',
      'Ember Data Model: translation for model name',
    );
  });
});
