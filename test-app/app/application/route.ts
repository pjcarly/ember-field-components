import Route from '@ember/routing/route';
import type Transition from '@ember/routing/transition';
import { inject as service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';

export default class ApplicationRoute extends Route {
  @service declare intl: IntlService;

  beforeModel(transition: Transition) {
    super.beforeModel(transition);
    this.intl.setLocale(['en-001']); // we set the locale to english international
  }
}
