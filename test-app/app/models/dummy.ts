import Model, { attr } from '@ember-data/model';
import checkbox from '@getflights/ember-field-components/fields/checkbox';
import field from '@getflights/ember-field-components/decorators/field';
import text from '@getflights/ember-field-components/fields/text';
import checkboxSwitch from '@getflights/ember-field-components/fields/checkbox-switch';

export default class DummyModel extends Model {
  @attr()
  @field(text())
  declare name: string;

  @attr()
  @field(checkbox({ prefix: 'test' }))
  declare verified?: boolean;

  @attr()
  @field(checkboxSwitch())
  declare active?: boolean;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    dummy: DummyModel;
  }
}
