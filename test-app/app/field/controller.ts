import Controller from '@ember/controller';
import field from '@getflights/ember-field-components/decorators/field';
import { tracked } from 'tracked-built-ins';
import text from '@getflights/ember-field-components/fields/text';
import { service } from '@ember/service';
import type Store from '@ember-data/store';
import number from '@getflights/ember-field-components/fields/number';
import price from '@getflights/ember-field-components/fields/price';
import email from '@getflights/ember-field-components/fields/email';

export default class FieldController extends Controller {
  @service declare store: Store;

  constructor(...args: never[]) {
    super(...args);

    this.dummy = this.store.createRecord('dummy');
    this.dummy.errors.add('name', 'Error on name');
    this.dummy.errors.add('verified', 'Error on verified');
    this.dummy.errors.add('active', 'Error on active');
  }

  dummy;

  @field(text({ prefix: 'abc', readonly: true }))
  @tracked
  name: string = 'Karel';

  preSetHook = (newName: string) => {
    return newName;
  };

  @field(text({ prefix: 'He is called...' }))
  @tracked
  firstName?: string;

  // @field(text({ prefix: 'Some call him...' }))
  @tracked
  nickName?: string;

  @field(
    number({
      prefix: 'model prefix',
      suffix: 'model suffix',
      inputOptions: { precision: 5 },
      readonly: true,
    }),
  )
  @tracked
  age?: number = 22;

  @field(
    price({
      suffix: 'model suffix',
    }),
  )
  @tracked
  price?: number = 50;

  // @field(percent({ suffix: 'percentage' }))
  @tracked
  percentage?: number = 50;

  @field(email({ suffix: 'UTC+1' }))
  @tracked
  email = 'k.bousson@getflights.com';
}
